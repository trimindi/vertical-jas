package com.trimindi.switching.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.RestListener;
import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.servindo.PaketData;
import com.trimindi.switching.utils.constanta.TStatus;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOPackager;
import org.jpos.util.LogSource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by PC on 21/07/2017.
 */
@Path("servindo")
public class ServindoControllers extends RestListener implements LogSource, Configurable {


    private final ObjectMapper objectMapper;

    public ServindoControllers(ISOPackager packager) {
        super(packager);
        this.objectMapper = new ObjectMapper();

    }

    @POST
    @Path("report")
    @Consumes({MediaType.TEXT_PLAIN})
    public Response report(String data){
        PaketData paketData = null;
        try {
            paketData = objectMapper.readValue(data,PaketData.class);
        } catch (IOException e) {
            return Response.status(200).entity("ERROR").build();
        }
        if(paketData == null){
            if(paketData.getReg_id() != null && !paketData.getReg_id().isEmpty()){
                Transaksi transaksi = transaksiService.findByHostRefId(paketData.getReg_id());
                if (transaksi != null){
                    switch (paketData.getStatus()){
                        case 1:
                            try {
                                transaksi.setST(TStatus.PAYMENT_SUCCESS)
                                        .setPAYMENT(objectMapper.writeValueAsString(paketData))
                                        .setBILL_REF_NUMBER(paketData.getSn());
                                transaksiService.update(transaksi);
                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 2:
                            try {
                                transaksi.setST(TStatus.PAYMENT_PROSESS)
                                        .setPAYMENT(objectMapper.writeValueAsString(paketData))
                                        .setBILL_REF_NUMBER(paketData.getSn());
                                transaksiService.update(transaksi);
                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                            break;
                        default:
                            try {
                                transaksi.setST(TStatus.PAYMENT_FAILED)
                                        .setPAYMENT(objectMapper.writeValueAsString(paketData))
                                        .setBILL_REF_NUMBER(paketData.getSn());
                                transaksiService.update(transaksi);
                                partnerDepositService.reverse(transaksi,objectMapper.writeValueAsString(paketData));
                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                    return Response.status(200).entity("OK").build();
                }else{
                    return Response.status(200).build();
                }
            }
        }
        return Response.status(200).build();
    }
    @Override
    public void setConfiguration(Configuration configuration) throws ConfigurationException {

    }
}
