package com.trimindi.switching.controllers;

import com.trimindi.switching.RestListener;
import com.trimindi.switching.models.*;
import com.trimindi.switching.packager.PLNPackager;
import com.trimindi.switching.rajabiller.RajaBillerService;
import com.trimindi.switching.response.PartnerDepositResponse;
import com.trimindi.switching.response.ReportResponse;
import com.trimindi.switching.response.postpaid.Payment;
import com.trimindi.switching.response.postpaid.PaymentResponse;
import com.trimindi.switching.response.postpaid.Rincian;
import com.trimindi.switching.services.TransaksiDepositService;
import com.trimindi.switching.servindo.ServindoService;
import com.trimindi.switching.thread.nontaglist.NontaglistInquiry;
import com.trimindi.switching.thread.nontaglist.NontaglistPayment;
import com.trimindi.switching.thread.postpaid.PostpaidInquiry;
import com.trimindi.switching.thread.postpaid.PostpaidPayment;
import com.trimindi.switching.thread.prepaid.PrepaidInquiry;
import com.trimindi.switching.thread.prepaid.PrepaidPayment;
import com.trimindi.switching.utils.PostpaidHelper;
import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.constanta.Constanta;
import com.trimindi.switching.utils.constanta.ResponseCode;
import com.trimindi.switching.utils.constanta.TStatus;
import com.trimindi.switching.utils.generator.BaseHelper;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.ParsingHelper;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.util.LogSource;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by PC on 6/17/2017.
 */
@Path("")
public class TransaksiControllers extends RestListener implements LogSource, Configurable {

    private static final String LOG = TransaksiControllers.class.getSimpleName();
    private static Map<String, String> nonTransaksi = new HashMap<>();
    ThreadPoolExecutor inquiryExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(6);
    ThreadPoolExecutor paymentExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(6);

    static {
        nonTransaksi.put("1", "PARTNER.CHECK.DEPOSIT");
        nonTransaksi.put("2", "PARTNER.TRANSAKSI");
        nonTransaksi.put("3", "PLN.CETAK.ULANG");
        nonTransaksi.put("4", "PARTNER.CHECK.DEPOSIT.LIST");
        nonTransaksi.put("4", "PARTNER.PRODUCT");
    }

    private final ServindoService servindoService;

    private RajaBillerService rajaBillerService;
    private TransaksiDepositService transaksiDepositService;
    private Transaksi transaksi;

    public TransaksiControllers(ISOPackager packager) {
        super(packager);
        this.transaksiDepositService = new TransaksiDepositService();
        this.rajaBillerService = new RajaBillerService();
        this.servindoService = new ServindoService();
    }

    @Override
    public void setConfiguration(Configuration configuration) throws ConfigurationException {

    }

    @POST
    @Path("")
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML,MediaType.TEXT_PLAIN})
    public void transaksi(@Suspended final AsyncResponse asyncResponse,
                        @Context ContainerRequestContext security,@Valid Request req) {
        try {
            PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
            PartnerPermision permision = (PartnerPermision) security.getProperty(Constanta.PERMISION);
            Map<String, String> params = new HashMap<>();
            if(nonTransaksi.containsValue(req.ACTION)){

            }else{
                if(validate(req,asyncResponse)){
                    return;
                }
                req.BACK_LINK = p.getBack_link();
                params.put(Constanta.MACHINE_TYPE,p.getMerchant_type());
                params.put(Constanta.MSSIDN,req.MSSIDN);
                params.put(Constanta.AREA , req.AREA);
                params.put(Constanta.TID,req.TID);
                params.put(Constanta.IP_ADDRESS, String.valueOf(security.getProperty(Constanta.IP_ADDRESS)));
                params.put(Constanta.MAC, String.valueOf(security.getProperty(Constanta.MAC)));
                params.put(Constanta.LONGITUDE, String.valueOf(security.getProperty(Constanta.LONGITUDE)));
                params.put(Constanta.LATITUDE, String.valueOf(security.getProperty(Constanta.LATITUDE)));
                params.put(Constanta.DENOM, req.PRODUCT);
                params.put(Constanta.BACK_LINK, req.BACK_LINK);
                params.put(Constanta.PID, req.PID);
            }
            ProductItem productItem = null;
            switch (req.ACTION){
                case "PLN.INQUIRY":
                    if(!permision.getPln()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PERMISION_USER_DENIED).build());
                        return;
                    }
                    productItem = productItemService.findByDenom(req.PRODUCT);
                    if(productItem == null){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                        return;
                    }
                    if(!productItem.getFk_biller().equalsIgnoreCase("001")){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE).build());
                        return;
                    }

                    switch (productItem.getProduct_id()){
                        case "99504":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_NONTAGLIST);
                            inquiryExecutor.execute(new NontaglistInquiry(asyncResponse,params,p,productItem));
                            break;
                        case "99502":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_PREPAID);
                            inquiryExecutor.execute(new PrepaidInquiry(asyncResponse,params,p, productItem));
                            break;
                        case "99501":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_POSTPAID);
                            inquiryExecutor.execute(new PostpaidInquiry(asyncResponse,params,p,productItem));
                            break;
                        default:
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                            break;
                    }

                    break;
                case "PLN.PAYMENT":
                    if(!permision.getPln()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PERMISION_USER_DENIED).build());
                        return;
                    }
                    if(checkNtrans(asyncResponse,req)){
                        return;
                    }
                    params.put(Constanta.NTRANS,req.NTRANS);
                    checkTransaksiPayment(params,asyncResponse);
                    if (transaksi == null) {
                        return;
                    }
                    bookingSaldo(transaksi,asyncResponse);
                    switch (transaksi.getPRODUCT()){
                        case "99501":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_POSTPAID);
                            paymentExecutor.execute(new PostpaidPayment(asyncResponse,transaksi,req));
                            break;
                        case "99504":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_NONTAGLIST);
                            paymentExecutor.execute(new NontaglistPayment(asyncResponse,transaksi,req));
                            break;
                        case "99502":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_PREPAID);
                            paymentExecutor.execute(new PrepaidPayment(asyncResponse, transaksi,req));
                            break;
                        default:
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                            break;
                    }
                    break;
                case "PLN.INQUIRY.INSTANSI":
                    if(!permision.getBigbill()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PERMISION_USER_DENIED).build());
                        return;
                    }
                    productItem = productItemService.findByDenom(req.PRODUCT);
                    if (productItem == null) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                        return;
                    }
                    if(!productItem.getFk_biller().equalsIgnoreCase("001")){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                    }
                    if (!productItem.isACTIVE()) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE).build());
                        return;
                    }
                    params.put(Constanta.INSTANSI, "true");
                    switch (productItem.getProduct_id()) {
                        case "99504":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_NONTAGLIST);
                            inquiryExecutor.execute(new NontaglistInquiry(asyncResponse, params, p, productItem));
                            break;
                        case "99502":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_PREPAID);
                            inquiryExecutor.execute(new PrepaidInquiry(asyncResponse, params, p, productItem));
                            break;
                        case "99501":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_POSTPAID);
                            inquiryExecutor.execute(new PostpaidInquiry(asyncResponse, params, p, productItem));
                            break;
                        default:
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                            break;
                    }
                    break;
                case "PLN.PAYMENT.INSTANSI":
                    if(!permision.getBigbill()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PERMISION_USER_DENIED).build());
                        return;
                    }
                    if (checkNtrans(asyncResponse, req)) {
                        return;
                    }
                    params.put(Constanta.NTRANS, req.NTRANS);
                    checkTransaksiPayment(params, asyncResponse);
                    if (transaksi == null) {
                        return;
                    }
                    transaksiService.updateToUnderProsess(transaksi);
                    params.put(Constanta.INSTANSI, "true");
                    switch (transaksi.getPRODUCT()) {
                        case "99501":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_POSTPAID);
                            paymentExecutor.execute(new PostpaidPayment(asyncResponse, transaksi, req));
                            break;
                        case "99504":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_NONTAGLIST);
                            paymentExecutor.execute(new NontaglistPayment(asyncResponse, transaksi, req));
                            break;
                        case "99502":
                            params.put(Constanta.PRODUCT_CODE, BaseHelper.PAN_PREPAID);
                            paymentExecutor.execute(new PrepaidPayment(asyncResponse, transaksi, req));
                            break;
                        default:
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                            break;
                    }
                    break;
                case "PLN.CETAK.ULANG":
                    if(checkNtrans(asyncResponse,req)){
                        return;
                    }
                    transaksi = transaksiService.findById(req.NTRANS);
                    if (transaksi == null) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND).build());
                        return;
                    }
                    cetakStruk(asyncResponse, transaksi);
                    break;
                case "PARTNER.CHECK.DEPOSIT.LIST":
                    List<TransaksiDeposit> transaksiDeposits = transaksiDepositService.findAll(p.getPartner_id(),req.BULAN,req.TAHUN);
                    PartnerDeposit PAR = partnerDepositService.findPartnerDeposit(p.getPartner_id());
                    PartnerDepositResponse partnerDepositResponse = new PartnerDepositResponse();
                    partnerDepositResponse.setPartnerDeposit(PAR).setTransaksiDeposits(transaksiDeposits)
                            .setTotalFee(transaksiService.getTotalFee(p.getPartner_id()))
                            .setStatus(true);
                    asyncResponse.resume(Response.status(200).entity(partnerDepositResponse).build());
                    break;
                case "PARTNER.CHECK.DEPOSIT":
                    PartnerDeposit PARD = partnerDepositService.findPartnerDeposit(p.getPartner_id());
                    asyncResponse.resume(Response.status(200).entity(PARD).build());
                    break;
                case "PARTNER.PRODUCT":
                    List<ProductItem> productItems = productItemService.findAll();
                    GenericEntity<List<ProductItem>> productCodeGenericEntity = new GenericEntity<List<ProductItem>>(productItems){};
                    asyncResponse.resume(Response.status(200).entity(productCodeGenericEntity).build());
                    break;
                case "PARTNER.TRANSAKSI":
                    if(req.BULAN.isEmpty() || req.BULAN.length() > 5 || req.TAHUN.isEmpty() || req.TAHUN.length() > 5){
                        asyncResponse.resume(Response.status(400).entity(ResponseCode.MISSING_REQUIRED_PARAMETER).build());
                    }
                    ReportResponse a = new ReportResponse();
                    GenericEntity<ReportResponse> tran = null;
                    try {
                        List<Transaksi> t = transaksiService.findByBulanDanTahun(req.BULAN,req.TAHUN,p.getPartner_uid(),req.STATUS,req.PRODUCT);
                        a.setStatus(true);
                        a.setTransaksiList(t);
                        tran = new GenericEntity<ReportResponse>(a){};
                    }catch (Exception e){
                        e.printStackTrace();
                        TLog.log(LOG + " - " + e.getMessage());
                    }
                    asyncResponse.resume(Response.status(200).entity(tran).build());
                    break;
                case "TELKOM.INQUIRY":
                    if(!permision.getTelkom()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PERMISION_USER_DENIED).build());
                        return;
                    }
                    productItem = productItemService.findByDenom(req.PRODUCT);
                    if(productItem == null){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                        return;
                    }
                    if(!productItem.getFk_biller().equalsIgnoreCase("002")){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE).build());
                        return;
                    }
                    rajaBillerService.InquiryTelkom(asyncResponse,productItem,params,p);
                    break;
                case "PDAM.INQUIRY":
                    if(!permision.getPdam()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PERMISION_USER_DENIED).build());
                        return;
                    }
                    productItem = productItemService.findByDenom(req.PRODUCT);
                    if(productItem == null){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                        return;
                    }
                    if(!productItem.getFk_biller().equalsIgnoreCase("003")){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE).build());
                        return;
                    }
                    rajaBillerService.InquiryPDAM(asyncResponse,productItem,params,p);
                    break;
                case "TELKOM.PAYMENT":
                    if(!permision.getTelkom()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PERMISION_USER_DENIED).build());
                        return;
                    }
                    if(checkNtrans(asyncResponse,req)){
                        return;
                    }
                    productItem = productItemService.findByDenom(req.PRODUCT);
                    if(productItem == null){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                        return;
                    }
                    if(!productItem.getFk_biller().equalsIgnoreCase("002")){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE).build());
                        return;
                    }
                    params.put(Constanta.NTRANS,req.NTRANS);
                    checkTransaksiPayment(params,asyncResponse);
                    if (transaksi == null) {
                        return;
                    }
                    bookingSaldo(transaksi,asyncResponse);
                    rajaBillerService.PaymentTelkom(asyncResponse, transaksi, params);
                    break;
                case "PDAM.PAYMENT":
                    if(!permision.getPdam()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PERMISION_USER_DENIED).build());
                        return;
                    }
                    if(checkNtrans(asyncResponse,req)){
                        return;
                    }
                    productItem = productItemService.findByDenom(req.PRODUCT);
                    if(productItem == null){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                        return;
                    }
                    if(!productItem.getFk_biller().equalsIgnoreCase("003")){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE).build());
                        return;
                    }
                    params.put(Constanta.NTRANS,req.NTRANS);
                    checkTransaksiPayment(params,asyncResponse);
                    if (transaksi == null) {
                        return;
                    }
                    bookingSaldo(transaksi,asyncResponse);
                    rajaBillerService.PaymentPDAM(asyncResponse,transaksi,params);
                    break;
                case "VOUCHER.PULSA":
                    if(!permision.getPulsa()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PERMISION_USER_DENIED).build());
                        return;
                    }
                    productItem = productItemService.findByDenom(req.PRODUCT);
                    if(productItem == null){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                        return;
                    }
                    if(!productItem.getFk_biller().equalsIgnoreCase("004")){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE).build());
                        return;
                    }

                    servindoService.beliPulsa(asyncResponse,productItem,params,p);
                    break;
                case "VOUCHER.DATA":
                    if(!permision.getPulsa()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PERMISION_USER_DENIED).build());
                        return;
                    }
                    productItem = productItemService.findByDenom(req.PRODUCT);
                    if(productItem == null){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                        return;
                    }
                    if(!productItem.getFk_biller().equalsIgnoreCase("005")){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE).build());
                        return;
                    }
                    servindoService.beliData(asyncResponse,productItem,params,p);
                    break;
                default:
                    asyncResponse.resume(Response.status(200).entity(ResponseCode.UNKNOW_ACTION).build());
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            TLog.log(LOG + " - " + e.getMessage());
            asyncResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build());
        }
    }
    private boolean checkNtrans(AsyncResponse asyncResponse, Request req) {
        if(req.NTRANS == null){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND).build());
            return true;
        }
        return false;
    }

    private void cetakStruk(@Suspended AsyncResponse asyncResponse, Transaksi transaksi) {
        if(Objects.equals(transaksi.getST(), TStatus.PAYMENT_SUCCESS)){
            if(transaksi.getPRT() >= 3){
                asyncResponse.resume(Response.status(200).entity(ResponseCode.MAXIMUM_PRINTED_RECIPT).build());
            }else{
                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(new PLNPackager());
                try {
                    isoMsg.unpack(transaksi.getPAYMENT().getBytes());
                } catch (ISOException e) {
                    TLog.log(LOG + " - "+ e.getNested());
                    asyncResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build());
                }
                List<Rules> rules;
                switch (transaksi.getPRODUCT()){
                    case "99501":
                        rules = ParsingHelper.parsingRulesPostPaid(isoMsg,true);
                        Payment payment = new Payment(rules);
                        String bit48= isoMsg.getString(48);
                        int legth = new SDE.Builder().setPayload(bit48).setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48,true)).calculate();
                        String rincian = bit48.substring(legth,bit48.length());
                        int start = 0;
                        int leghtRincian = 115;
                        List<Rincian> rincians = new ArrayList<>();
                        for(int i = 0; i< payment.getBillStatus(); i++){
                            String parsRincian = rincian.substring(start,start+leghtRincian);
                            List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
                            Rincian r = new Rincian(rc);
                            rincians.add(r);
                            start += leghtRincian;
                        }
                        payment.setPeriod(PostpaidHelper.generatePeriode(rincians));
                        payment.setMeter(PostpaidHelper.generateStandMeter(rincians));
                        PaymentResponse baseResponse = new PaymentResponse();
                        baseResponse.setTagihan(transaksi.getAMOUT());
                        baseResponse.setTotalBayar(transaksi.getCHARGE());
                        payment.setRincians(rincians);
                        baseResponse.setData(payment);
                        baseResponse.setNtrans(transaksi.getNTRANS());
                        transaksi.setPRT(transaksi.getPRT() + 1);
                        transaksiService.update(transaksi);
                        asyncResponse.resume(Response.status(200).entity(baseResponse).build());
                        break;
                    case "99502":
                        rules = ParsingHelper.parsingRulesPrepaid(isoMsg,true);
                        com.trimindi.switching.response.prepaid.Payment prepaid = new com.trimindi.switching.response.prepaid.Payment(rules,true);
                        com.trimindi.switching.response.prepaid.PaymentResponse baseResponse1 = new com.trimindi.switching.response.prepaid.PaymentResponse();
                        baseResponse1.setData(prepaid);
                        baseResponse1.setTotalBayar(transaksi.getCHARGE());
                        baseResponse1.setTagihan(transaksi.getAMOUT());
                        baseResponse1.setNtrans(transaksi.getNTRANS());
                        transaksi.setPRT(transaksi.getPRT() + 1);
                        transaksiService.update(transaksi);
                        asyncResponse.resume(Response.status(200).entity(baseResponse1).build());
                        break;
                    case "99504":
                        rules = ParsingHelper.parsingRulesNontaglist(isoMsg,true);
                        com.trimindi.switching.response.nontaglist.Payment nontaglist = new com.trimindi.switching.response.nontaglist.Payment(rules);
                        com.trimindi.switching.response.nontaglist.PaymentResponse baseResponse2 = new com.trimindi.switching.response.nontaglist.PaymentResponse();
                        baseResponse2.setTotalBayar(transaksi.getCHARGE());
                        baseResponse2.setTagihan(transaksi.getAMOUT());
                        baseResponse2.setNtrans(transaksi.getNTRANS());
                        baseResponse2.setData(nontaglist);
                        transaksi.setPRT(transaksi.getPRT() + 1);
                        transaksiService.update(transaksi);
                        asyncResponse.resume(Response.status(200).entity(baseResponse2).build());
                }
            }
        }
        asyncResponse.resume(Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND).build());
    }

    private void bookingSaldo(Transaksi transaksi,AsyncResponse asyncResponse) {
        if(partnerDepositService.bookingSaldo(transaksi)){
            transaksiService.updateToUnderProsess(transaksi);
        }else{
            if(asyncResponse.isSuspended()){
                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO).build());
                return;
            }
        }
    }

    private void checkTransaksiInquiry(Map<String,String> params, AsyncResponse asyncResponse) {
        transaksi = transaksiService.findByMSSIDN(params.get(Constanta.MSSIDN));
        if(transaksi != null) {
            if (transaksi.getST().equals(TStatus.PAYMENT_PROSESS)) {
                asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_UNDER_PROSES).build());
            }
        }
    }
    private void checkTransaksiPayment(Map<String,String> params, AsyncResponse asyncResponse) {
        transaksi = transaksiService.findById(params.get(Constanta.NTRANS));
        if(transaksi == null){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND).build());
            return;
        }
        if (Objects.equals(transaksi.getST(), TStatus.PAYMENT_PROSESS)) {
            asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_UNDER_PROSES).build());
            return;
        }
        if (Objects.equals(transaksi.getST(), TStatus.PAYMENT_FAILED)) {
            asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_FAILED).build());
            return;
        }
        if (Objects.equals(transaksi.getST(), TStatus.PAYMENT_SUCCESS)) {
            asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_DONE).build());
            return;
        }
    }

    private boolean validate(Request req, AsyncResponse asyncResponse) {
        if (req.ACTION == null || req.ACTION.isEmpty()){
            asyncResponse.resume(Response.status(200).entity(new ResponseCode("2001","Missin ACTION")).build());
            return true;
        }
        if (req.PRODUCT == null || req.PRODUCT.isEmpty()){
            asyncResponse.resume(Response.status(200).entity(new ResponseCode("2002","Missin PRODUCT")).build());
            return true;
        }
        if (req.TID == null || req.TID.isEmpty() || req.TID.length() != 16){
            asyncResponse.resume(Response.status(200).entity(new ResponseCode("2001","Missin TID")).build());
            return true;
        }
        if (req.PID == null || req.PID.isEmpty() || req.PID.length() != 7){
            asyncResponse.resume(Response.status(200).entity(new ResponseCode("2001","Missin PID")).build());
            return true;
        }

        return false;
    }
}

