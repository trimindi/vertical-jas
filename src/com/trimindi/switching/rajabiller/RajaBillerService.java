package com.trimindi.switching.rajabiller;

import com.trimindi.switching.models.*;
import com.trimindi.switching.rajabiller.response.MethodResponse;
import com.trimindi.switching.response.pdam.Inquiry;
import com.trimindi.switching.response.pdam.InquiryResponse;
import com.trimindi.switching.response.pdam.Payment;
import com.trimindi.switching.response.pdam.PaymentResponse;
import com.trimindi.switching.response.telkom.PaymentResponseTelkom;
import com.trimindi.switching.response.telkom.PaymentTelkom;
import com.trimindi.switching.services.PartnerDepositService;
import com.trimindi.switching.services.ProductFeeService;
import com.trimindi.switching.services.ProductItemService;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.constanta.Constanta;
import com.trimindi.switching.utils.constanta.ResponseCode;
import com.trimindi.switching.utils.constanta.TStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Objects;

/**
 * Created by PC on 6/19/2017.
 */
public class RajaBillerService {
    private static HttpPost httpPost;
    private static JAXBContext jc;
    private static Unmarshaller unmarshaller;
    private TransaksiService transaksiService;
    private ProductItemService productItemService;
    private ProductFeeService productFeeService;
    private PartnerDepositService partnerDepositService;
    private CloseableHttpClient client;

    public RajaBillerService() {
        try {
            jc = JAXBContext.newInstance(MethodResponse.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            TLog.log("ERRR" + e.getMessage());
        }

        SSLContext sslContext = null;
        try {
            sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(null, new TrustStrategy() {
                        @Override
                        public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                            return true;
                        }
                    }).build();
            client = HttpClients.custom()
                    .setSSLContext(sslContext)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            e.printStackTrace();
        }
        httpPost = new HttpPost("https://202.43.173.234/transaksi/");
        this.transaksiService = new TransaksiService();
        this.productItemService = new ProductItemService();
        this.productFeeService = new ProductFeeService();
        this.partnerDepositService = new PartnerDepositService();
    }

    public void InquiryPDAM(final AsyncResponse toResponse, final ProductItem product, final Map<String,String> params, final PartnerCredential partnerCredential){
        final ProductFee productFee = productFeeService.findFee(partnerCredential.getPartner_id(),product.getDENOM());
        try {
            StringEntity entity = null;
            entity = new StringEntity(RajabilerRequest.inquiryPDAM(product.getDENOM(),params.get(Constanta.MSSIDN),params.get(Constanta.MSSIDN),params.get(Constanta.MSSIDN),params.get(Constanta.NTRANS)));
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);

            String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            System.out.println(respone);
            TLog.log("INQUIRY PDAM" + respone);
            MethodResponse methodResponse = null;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                Inquiry inquiry = new Inquiry(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(partnerCredential.getPartner_id());
                switch (Integer.parseInt(inquiry.getSTATUS())) {
                    case 0:
                        if(inquiry.getNOMINAL() == 0){
                            toResponse.resume(Response.status(200).entity(new ResponseCode("7890","Tidak sudah terbayar")).build());
                            return;
                        }
                        Transaksi transaksi = new Transaksi()
                                .setNTRANS(params.get(Constanta.NTRANS))
                                .setADMIN(inquiry.getBIAYAADMIN())
                                .setMSSIDN_NAME(inquiry.getNAMAPELANGGAN())
                                .setHOST_REF_NUMBER(inquiry.getREF2())
                                .setBILL_REF_NUMBER(inquiry.getREF1())
                                .setMSSIDN(inquiry.getIDPELANGGAN1())
                                .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                .setPRODUCT(product.getProduct_id())
                                .setAMOUT(inquiry.getNOMINAL() - inquiry.getBIAYAADMIN())
                                .setDENOM(product.getDENOM())
                                .setFEE(productFee.getFEE())
                                .setUSERID(partnerCredential.getPartner_uid())
                                .setINQUIRY(respone)
                                .setCHARGE((inquiry.getNOMINAL() - (inquiry.getBIAYAADMIN() - productFee.getFEE())))
                                .setDEBET((inquiry.getNOMINAL() - (inquiry.getBIAYAADMIN() - productFee.getFEE())))
                                .setMAC_ADDRESS(params.get(Constanta.MAC))
                                .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                .setPARTNERID(partnerCredential.getPartner_id())
                                .setLATITUDE(params.get(Constanta.LATITUDE))
                                .setLONGITUDE(params.get(Constanta.LONGITUDE))
                                .setPINALTY(0)
                                .setST(TStatus.INQUIRY);
                        transaksiService.persist(transaksi);
                        InquiryResponse inquiryResponse = new InquiryResponse();
                        inquiryResponse.setData(inquiry);
                        inquiryResponse.setFee(transaksi.getFEE()).setNtrans(params.get(Constanta.NTRANS));
                        inquiryResponse.setSaldo(partnerDeposit.getBALANCE());
                        inquiryResponse.setTotalFee(transaksiService.getTotalFee(partnerCredential.getPartner_id()));
                        inquiryResponse.setTagihan(transaksi.getAMOUT());
                        inquiryResponse.setTotalBayar(transaksi.getCHARGE());
                        toResponse.resume(Response.status(200).entity(inquiryResponse).build());
                        break;
                    default:
                        toResponse.resume(Response.status(200).entity(new ResponseCode(inquiry.getSTATUS(),inquiry.getKETERANGAN())).build());
                        break;
                }
            } catch (JAXBException e) {
                TLog.log("ERRR" + e.getMessage());
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build());
            }
        } catch (IOException e) {
            TLog.log("ERRR" + e.getMessage());
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build());
        }
    }
    public void PaymentPDAM(AsyncResponse toResponse, Transaksi transaksi, Map<String,String> params){
        Payment payment = null;
        String respone = null;
        try{
            StringEntity entity = new StringEntity(RajabilerRequest.paymentPDAM(transaksi));
            System.out.println(entity);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);
            respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            System.out.println(respone);
            TLog.log("PAYMENT PDAM" + respone);
            MethodResponse methodResponse = null;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                payment = new Payment(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(transaksi.getPARTNERID());
                switch (Integer.parseInt(payment.getSTATUS())) {
                    case 0:
                        transaksi.setST(TStatus.PAYMENT_SUCCESS)
                                .setPAYMENT(respone)
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        PaymentResponse paymentResponse = new PaymentResponse();
                        paymentResponse.setData(payment);
                        paymentResponse.setFee(transaksi.getFEE());
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setTagihan(transaksi.getAMOUT());
                        paymentResponse.setTotalBayar(transaksi.getCHARGE());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setTotalFee(transaksiService.getTotalFee(transaksi.getPARTNERID()));
                        toResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    default:
                        partnerDepositService.reverse(transaksi,respone);
                        toResponse.resume(Response.status(200).entity(new ResponseCode(payment.getSTATUS(),payment.getKETERANGAN())).build());
                        break;
                }
            } catch (JAXBException e) {
                TLog.log("ERRR" + e.getMessage());
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build());
            }
        } catch (IOException e) {
            TLog.log("ERRR" + e.getMessage());
            partnerDepositService.reverse(transaksi,respone);
        }
    }
    public void InquiryTelkom(final AsyncResponse toResponse, final ProductItem product, final Map<String,String> params, final PartnerCredential partnerCredential){
        final ProductFee productFee = productFeeService.findFee(partnerCredential.getPartner_id(),product.getDENOM());
        try {
            StringEntity entity = null;
            entity = new StringEntity(RajabilerRequest.inquiryTelkom(product.getDENOM(),params.get(Constanta.AREA),params.get(Constanta.MSSIDN),params.get(Constanta.NTRANS),product.getDENOM().equalsIgnoreCase("SPEEDY")));
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            System.out.println(RajabilerRequest.inquiryTelkom(product.getDENOM(), params.get(Constanta.AREA), params.get(Constanta.MSSIDN), params.get(Constanta.NTRANS), product.getDENOM().equalsIgnoreCase("SPEEDY")));
            CloseableHttpResponse response = client.execute(httpPost);
            String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            System.out.println(respone);
            TLog.log("INQUIRY TELKOM" + respone);
            MethodResponse methodResponse = null;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                Inquiry inquiry = new Inquiry(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(partnerCredential.getPartner_id());
                switch (Integer.parseInt(inquiry.getSTATUS())) {
                    case 0:
                        if(inquiry.getNOMINAL() == 0){
                            toResponse.resume(Response.status(200).entity(new ResponseCode("7890","Tidak sudah terbayar")).build());
                            return;
                        }
                        Transaksi transaksi = new Transaksi()
                                .setNTRANS(params.get(Constanta.NTRANS))
                                .setADMIN(inquiry.getBIAYAADMIN())
                                .setMSSIDN_NAME(inquiry.getNAMAPELANGGAN())
                                .setHOST_REF_NUMBER(inquiry.getREF2())
                                .setBILL_REF_NUMBER(inquiry.getREF1())
                                .setMSSIDN(inquiry.getIDPELANGGAN1())
                                .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                .setPRODUCT(product.getProduct_id())
                                .setAMOUT((inquiry.getNOMINAL() - inquiry.getBIAYAADMIN()))
                                .setDENOM(product.getDENOM())
                                .setFEE(productFee.getFEE())
                                .setUSERID(partnerCredential.getPartner_uid())
                                .setINQUIRY(respone)
                                .setCHARGE((inquiry.getNOMINAL() - (inquiry.getBIAYAADMIN() - productFee.getFEE())))
                                .setDEBET((inquiry.getNOMINAL() - (inquiry.getBIAYAADMIN() - productFee.getFEE())))
                                .setMAC_ADDRESS(params.get(Constanta.MAC))
                                .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                .setPARTNERID(partnerCredential.getPartner_id())
                                .setLATITUDE(params.get(Constanta.LATITUDE))
                                .setLONGITUDE(params.get(Constanta.LONGITUDE))
                                .setPINALTY(0)
                                .setST(TStatus.INQUIRY);
                        transaksiService.persist(transaksi);
                        InquiryResponse inquiryResponse = new InquiryResponse();
                        inquiryResponse.setData(inquiry);
                        inquiryResponse.setFee(transaksi.getFEE()).setNtrans(params.get(Constanta.NTRANS));
                        inquiryResponse.setSaldo(partnerDeposit.getBALANCE());
                        inquiryResponse.setTotalFee(transaksiService.getTotalFee(partnerCredential.getPartner_id()));
                        inquiryResponse.setTagihan(transaksi.getAMOUT());
                        inquiryResponse.setTotalBayar(transaksi.getCHARGE());
                        toResponse.resume(Response.status(200).entity(inquiryResponse).build());
                        break;
                    default:
                        toResponse.resume(Response.status(200).entity(new ResponseCode(inquiry.getSTATUS(),inquiry.getKETERANGAN())).build());
                        break;
                }
            } catch (JAXBException e) {
                TLog.log("ERRR" + e.getMessage());
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build());
            }
        } catch (IOException e) {
            TLog.log("ERRR" + e.getMessage());
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build());
        }
    }
    public void PaymentTelkom(AsyncResponse toResponse, Transaksi transaksi, Map<String,String> params){
        String respone = null;
        PaymentTelkom payment = null;
        try{
            StringEntity entity = new StringEntity(RajabilerRequest.paymentTelokm(transaksi));
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);
            respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            System.out.println(respone);
            TLog.log("PAYMENT TELKOM" + respone);
            MethodResponse methodResponse = null;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                payment = new PaymentTelkom(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(transaksi.getPARTNERID());
                switch (Integer.parseInt(payment.getSTATUS())) {
                    case 0:
                        transaksi.setST(TStatus.PAYMENT_SUCCESS)
                                .setPAYMENT(respone)
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        PaymentResponseTelkom paymentResponse = new PaymentResponseTelkom();
                        paymentResponse.setData(payment);
                        paymentResponse.setFee(transaksi.getFEE());
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setTagihan(transaksi.getAMOUT());
                        paymentResponse.setTotalBayar(transaksi.getCHARGE());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setTotalFee(transaksiService.getTotalFee(transaksi.getPARTNERID()));
                        toResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    default:
                        partnerDepositService.reverse(transaksi,respone);
                        toResponse.resume(Response.status(200).entity(new ResponseCode(payment.getSTATUS(),payment.getKETERANGAN())).build());
                        break;
                }
            } catch (JAXBException e) {
                TLog.log("ERRR" + e.getMessage());
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build());
            }
        } catch (IOException e) {
            TLog.log("ERRR" + e.getMessage());
            partnerDepositService.reverse(transaksi,respone);
        }
    }
    public void beliPulsa(AsyncResponse toResponse, ProductItem product, Map<String, String> params, PartnerCredential partnerCredential) {
        final ProductFee productFee = productFeeService.findFee(partnerCredential.getPartner_id(),product.getDENOM());
        Transaksi transaksi = null;
        try {
            transaksi = new Transaksi()
                    .setNTRANS(params.get(Constanta.NTRANS))
                    .setADMIN(0)
                    .setMSSIDN_NAME(params.get(Constanta.MSSIDN))
                    .setBILL_REF_NUMBER(params.get(Constanta.NTRANS))
                    .setMSSIDN(params.get(Constanta.MSSIDN))
                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                    .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                    .setPRODUCT(product.getProduct_id())
                    .setAMOUT(product.getAMOUT())
                    .setDENOM(product.getDENOM())
                    .setFEE(productFee.getFEE())
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setCHARGE(product.getAMOUT())
                    .setDEBET(product.getAMOUT())
                    .setMAC_ADDRESS(params.get(Constanta.MAC))
                    .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setLATITUDE(params.get(Constanta.LATITUDE))
                    .setLONGITUDE(params.get(Constanta.LONGITUDE))
                    .setPINALTY(0)
                    .setST(TStatus.PAYMENT_PROSESS);
            transaksiService.persist(transaksi);
            if(partnerDepositService.bookingSaldo(transaksi)){
                StringEntity entity = null;
                entity = new StringEntity(RajabilerRequest.buyPulsa(product.getDENOM(),params.get(Constanta.MSSIDN),params.get(Constanta.NTRANS)));
                httpPost.setEntity(entity);
                httpPost.setHeader("Accept", "application/xml");
                httpPost.setHeader("Content-type", "application/xml");
                CloseableHttpResponse response = client.execute(httpPost);

                String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
                System.out.println(respone);
                TLog.log("BELI PULSA" + respone);
                MethodResponse methodResponse = null;
                StringReader stringReader = new StringReader(respone);
                try {
                    methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                    com.trimindi.switching.response.pulsa.Payment payment = new com.trimindi.switching.response.pulsa.Payment(methodResponse);
                    PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(partnerCredential.getPartner_id());
                    if(Objects.equals(payment.getSTATUS(), "")){
                        payment.setSTATUS("0");
                    }
                    switch (Integer.parseInt(payment.getSTATUS())) {
                        case 0:
                            transaksi.setST(TStatus.PAYMENT_SUCCESS)
                                    .setPAYMENT(respone)
                                    .setBILL_REF_NUMBER(payment.getREF2())
                                    .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                            transaksiService.update(transaksi);
                            com.trimindi.switching.response.pulsa.PaymentResponse inquiryResponse = new com.trimindi.switching.response.pulsa.PaymentResponse();
                            inquiryResponse.setData(payment);
                            inquiryResponse.setNtrans(params.get(Constanta.NTRANS));
                            inquiryResponse.setSaldo(partnerDeposit.getBALANCE());
                            inquiryResponse.setTotalFee(transaksiService.getTotalFee(partnerCredential.getPartner_id()));
                            inquiryResponse.setTotalBayar(transaksi.getCHARGE());
                            toResponse.resume(Response.status(200).entity(inquiryResponse).build());
                            break;
                        default:
                            partnerDepositService.reverse(transaksi,respone);
                            toResponse.resume(Response.status(200).entity(new ResponseCode(payment.getSTATUS(),payment.getKETERANGAN())).build());
                            break;
                    }
                } catch (JAXBException e) {
                    TLog.log("ERRR" + e.getMessage());
                    toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build());
                }
            }else{
                toResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO).build());
                return;
            }
        } catch (IOException e) {
            TLog.log("ERRR" + e.getMessage());
            partnerDepositService.reverse(transaksi,"");
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build());
        }
    }

}
