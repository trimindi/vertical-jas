package com.trimindi.switching.rajabiller.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/18/2017.
 */
@XmlRootElement
public class Array
{
    private Data data;

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public Array() {
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+"]";
    }
}
