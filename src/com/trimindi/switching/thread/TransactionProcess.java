package com.trimindi.switching.thread;

import com.trimindi.switching.models.*;
import com.trimindi.switching.packager.PLNPackager;
import com.trimindi.switching.response.prepaid.Inquiry;
import com.trimindi.switching.response.prepaid.InquiryResponse;
import com.trimindi.switching.services.PartnerDepositService;
import com.trimindi.switching.services.ProductFeeService;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.utils.constanta.Constanta;
import com.trimindi.switching.utils.constanta.TStatus;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPrePaid;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * Created by HP on 19/05/2017.
 */
public class TransactionProcess {
    private TransaksiService transaksiService;
    private ProductFeeService productFeeService;
    private PartnerDepositService partnerDepositService;

    public TransactionProcess(){
        transaksiService = new TransaksiService();
        productFeeService = new ProductFeeService();
        partnerDepositService = new PartnerDepositService();
    }

    public InquiryResponse inquiryPrepaid(ISOMsg inquiry, PartnerCredential partnerCredential, Map<String,String> map, ProductItem productItem) throws ISOException {
        inquiry.setPackager(new PLNPackager());
        Transaksi transaksi = new Transaksi();

        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getString(48))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(48,true))
                .generate();

        List<Rules> bit62 = new SDE.Builder()
                .setPayload(inquiry.getString(62))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(62,true))
                .generate();
        bit48.addAll(bit62);
        Inquiry response = new Inquiry(bit48,true);
        /**
         * MT = MACHINE_TYPE
         * PC = PRODUDUCT CODE
         * PRODUCT = PRODUCT
         */
        ProductFee productFee = productFeeService.findFee(partnerCredential.getPartner_id(),map.get(Constanta.DENOM));
        PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(partnerCredential.getPartner_id());
        if (map.get(Constanta.INSTANSI) != null) {
            transaksi
                    .setSTAN(inquiry.getString(11))
                    .setADMIN(response.getAdmin())
                    .setMSSIDN_NAME(response.getSubscriberName())
                    .setBILL_REF_NUMBER(response.getBukopinReferenceNumber())
                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                    .setMSSIDN(response.getSubscriberID())
                    .setMERCHANT_ID(map.get(Constanta.MACHINE_TYPE))
                    .setAMOUT(productItem.getAMOUT())
                    .setFEE(productFee.getFEE())
                    .setPRODUCT(productItem.getProduct_id())
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setDENOM(productItem.getDENOM())
                    .setINQUIRY(new String(inquiry.pack()))
                    .setCHARGE(0)
                    .setDEBET(0)
                    .setMAC_ADDRESS(map.get(Constanta.MAC))
                    .setIP_ADDRESS(map.get(Constanta.IP_ADDRESS))
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setLATITUDE(map.get(Constanta.LATITUDE))
                    .setLONGITUDE(map.get(Constanta.LONGITUDE))
                    .setPINALTY(0)
                    .setST(TStatus.INQUIRY);
        } else {
            transaksi
                    .setSTAN(inquiry.getString(11))
                    .setADMIN(response.getAdmin())
                    .setMSSIDN_NAME(response.getSubscriberName())
                    .setBILL_REF_NUMBER(response.getBukopinReferenceNumber())
                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                    .setMSSIDN(response.getSubscriberID())
                    .setMERCHANT_ID(map.get(Constanta.MACHINE_TYPE))
                    .setAMOUT(productItem.getAMOUT())
                    .setFEE(productFee.getFEE())
                    .setPRODUCT(productItem.getProduct_id())
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setDENOM(productItem.getDENOM())
                    .setINQUIRY(new String(inquiry.pack()))
                    .setCHARGE(productItem.getAMOUT() + productItem.getADMIN() - productFee.getFEE())
                    .setDEBET(productItem.getAMOUT() + productItem.getADMIN() - productFee.getFEE())
                    .setMAC_ADDRESS(map.get(Constanta.MAC))
                    .setIP_ADDRESS(map.get(Constanta.IP_ADDRESS))
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setLATITUDE(map.get(Constanta.LATITUDE))
                    .setLONGITUDE(map.get(Constanta.LONGITUDE))
                    .setPINALTY(0)
                    .setST(TStatus.INQUIRY);
        }
        transaksiService.persist(transaksi);
        response.setTagihan(transaksi.getAMOUT());
        InquiryResponse baseResponse = new InquiryResponse();
        baseResponse.setFee(productItem.getDEFAULT_FEE()).setNtrans(transaksi.getNTRANS());
        baseResponse.setSaldo(partnerDeposit.getBALANCE());
        baseResponse.setTagihan(transaksi.getAMOUT());
        baseResponse.setTotalBayar(transaksi.getCHARGE());
        baseResponse.setTotalFee(transaksiService.getTotalFee(partnerCredential.getPartner_id()));
        baseResponse.setData(response);
        return baseResponse;
    }

}
