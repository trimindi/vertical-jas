package com.trimindi.switching.thread.prepaid;

import com.trimindi.switching.client.ChannelManager;
import com.trimindi.switching.models.PartnerCredential;
import com.trimindi.switching.models.ProductItem;
import com.trimindi.switching.response.prepaid.InquiryResponse;
import com.trimindi.switching.thread.BaseThread;
import com.trimindi.switching.thread.TransactionProcess;
import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.constanta.Constanta;
import com.trimindi.switching.utils.constanta.ResponseCode;
import com.trimindi.switching.utils.generator.PrePaidGenerator;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.NameRegistrar;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 15/05/2017.
 */
public class PrepaidInquiry extends BaseThread implements Runnable {
    private static final String LOG = PrepaidInquiry.class.getSimpleName();
    private Map<String, String> params;
    private AsyncResponse response;
    private ChannelManager channelManager;
    private PartnerCredential partnerCredential;
    private TransactionProcess transactionProcess;
    private ProductItem productItem;

    public PrepaidInquiry(AsyncResponse asyncResponse, Map<String ,String> request, PartnerCredential p, ProductItem productItem) {
        super();
        try {
            this.productItem = productItem;
            this.transactionProcess = new TransactionProcess();
            this.partnerCredential = p;
            this.params = request;
            this.response = asyncResponse;
            this.channelManager = (ChannelManager) NameRegistrar.get("manager");
            response.setTimeoutHandler(new TimeoutHandler() {
                @Override
                public void handleTimeout(AsyncResponse asyncResponse) {
                    sendBack(ResponseCode.SERVER_TIMEOUT);
                }
            });
            response.setTimeout(60, TimeUnit.SECONDS);
        } catch (NameRegistrar.NotFoundException e) {
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        }
    }

    @Override
    public void run() {
        super.run();

        ISOMsg inquiry;
        try {
            inquiry = channelManager.sendMsg(PrePaidGenerator.generateInquiry(params.get(Constanta.MACHINE_TYPE), params.get(Constanta.MSSIDN),params.get(Constanta.TID),params.get(Constanta.PID)));
            if (inquiry != null) {
                if(inquiry.getString(39).equals("0000")){
                    InquiryResponse prePaidInquiryResponse = transactionProcess.inquiryPrepaid(inquiry,partnerCredential,params, productItem);
                    sendBack(Response.status(200).entity(prePaidInquiryResponse).build());
                }else{
                    sendBack(Response.status(200).entity(responseCode.get(inquiry.getString(39))).build());
                }
            }
        } catch (ISOException e) {
            TLog.log(LOG + " - " + e.getMessage());
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        } catch (Exception e) {
            e.printStackTrace();
            TLog.log(LOG + " - " + e.getMessage());
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        }

    }
    private void sendBack(ResponseCode r){
        try{
            if(response.isSuspended()){
                response.resume(Response.status(200).entity(r).build());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    private void sendBack(Response r){
        try{
            if(response.isSuspended()){
                response.resume(r);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
