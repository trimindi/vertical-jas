package com.trimindi.switching.thread.nontaglist;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.client.ChannelManager;
import com.trimindi.switching.controllers.Request;
import com.trimindi.switching.models.PartnerDeposit;
import com.trimindi.switching.models.Transaksi;
import com.trimindi.switching.response.nontaglist.Payment;
import com.trimindi.switching.response.nontaglist.PaymentResponse;
import com.trimindi.switching.services.PartnerDepositService;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.thread.BaseThread;
import com.trimindi.switching.utils.TLog;
import com.trimindi.switching.utils.constanta.ResponseCode;
import com.trimindi.switching.utils.constanta.TStatus;
import com.trimindi.switching.utils.generator.NonTagListGenerator;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorNonTagList;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.NameRegistrar;

import javax.net.ssl.SSLContext;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 15/05/2017.
 */
public class NontaglistPayment extends BaseThread implements Runnable {
    private static final String LOG = NontaglistPayment.class.getSimpleName();
    private static HttpPost httpPost;
    private Request req;
    private AsyncResponse response;
    private ChannelManager channelManager;
    private Transaksi transaksi;
    private TransaksiService transaksiService;
    private PartnerDepositService partnerDepositService;
    private ObjectMapper objectMapper;
    private CloseableHttpClient client;

    public NontaglistPayment(AsyncResponse asyncResponse, final Transaksi transaksi, Request req) {
        super();
        try {
            this.req = req;
            this.transaksi = transaksi;
            this.transaksiService = new TransaksiService();
            this.partnerDepositService = new PartnerDepositService();
            this.response = asyncResponse;
            this.channelManager = (ChannelManager) NameRegistrar.get("manager");
            response.setTimeoutHandler(new TimeoutHandler() {
                @Override
                public void handleTimeout(AsyncResponse asyncResponse) {
                    sendBack(ResponseCode.PAYMENT_UNDER_PROSES.setNtrans(transaksi.getNTRANS()));
                }
            });
            response.setTimeout(60, TimeUnit.SECONDS);
            this.objectMapper = new ObjectMapper();
            SSLContext sslContext = null;
            try {
                sslContext = new SSLContextBuilder()
                        .loadTrustMaterial(null, new TrustStrategy() {
                            @Override
                            public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                                return true;
                            }
                        }).build();
                client = HttpClients.custom()
                        .setSSLContext(sslContext)
                        .setSSLHostnameVerifier(new NoopHostnameVerifier())
                        .build();
                httpPost = new HttpPost(req.BACK_LINK);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
        } catch (NameRegistrar.NotFoundException e) {
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setNtrans(transaksi.getNTRANS()));
        }
    }

    @Override
    public void run() {
        super.run();
        ISOMsg payment = null;
        List<Rules> rule;
        try {
            ISOMsg paymentRequest = NonTagListGenerator.generatePurchase(transaksi,req.TID,req.PID);
            payment = channelManager.sendMsg(paymentRequest);
            if (payment != null) {
                if(payment.getString(39).equals("0000")){
                    boolean status = true;
                    rule = parsingRules(payment, status);
                    Payment paymentResponse = new Payment(rule);
                    transaksi.setPAYMENT(new String(payment.pack())).setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()))
                            .setST(TStatus.PAYMENT_SUCCESS)
                            .setBILL_REF_NUMBER(paymentResponse.getBukopinReferenceNumber());
                    transaksiService.update(transaksi);
                    PaymentResponse baseResponse = new PaymentResponse();
                    paymentResponse.setTagihan(transaksi.getAMOUT());
                    PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(transaksi.getPARTNERID());
                    baseResponse.setData(paymentResponse);
                    baseResponse.setFee(transaksi.getFEE());
                    baseResponse.setSaldo(partnerDeposit.getBALANCE());
                    baseResponse.setTotalFee(transaksiService.getTotalFee(transaksi.getPARTNERID()));
                    baseResponse.setTagihan(transaksi.getAMOUT());
                    baseResponse.setTotalBayar(transaksi.getCHARGE());
                    baseResponse.setNtrans(transaksi.getNTRANS());
                    sendBack(
                            Response.status(200)
                                    .entity(baseResponse)
                                    .build()
                    );
                } else {
                    if (failedCode.containsKey(payment.getString(39))) {
                        payment = null;
                        ISOMsg reversal = NonTagListGenerator.generateReversal(paymentRequest, "2400");
                        payment = channelManager.sendMsg(reversal);
                        if (payment != null) {
                            if (payment.getString(39).equals("0000")) {
                                partnerDepositService.failed(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                            } else if (failedCode.containsKey(payment.getString(39))) {
                                reversal = NonTagListGenerator.generateReversal(paymentRequest, "2401");
                                payment = null;
                                payment = channelManager.sendMsg(reversal);
                                if (payment != null) {
                                    if (payment.getString(39).equals("0000")) {
                                        partnerDepositService.failed(transaksi, payment);
                                        sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                                    } else if(failedCode.containsKey(payment.getString(39))) {
                                        reversal = NonTagListGenerator.generateReversal(paymentRequest, "2401");
                                        payment = null;
                                        payment = channelManager.sendMsg(reversal);
                                        if (payment.getString(39).equals("0000")) {
                                            partnerDepositService.failed(transaksi, payment);
                                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                                        }else{
                                            partnerDepositService.failed(transaksi, payment);
                                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                                        }
                                    }else{
                                        partnerDepositService.failed(transaksi, payment);
                                        sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                                    }
                                }else{
                                    partnerDepositService.failed(transaksi, payment);
                                    sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                                }
                            } else {
                                partnerDepositService.failed(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                            }
                        }

                    } else {
                        partnerDepositService.failed(transaksi, payment);
                        sendBack(
                                Response.status(200)
                                        .entity(responseCode.get(payment.getString(39)))
                                        .build()
                        );
                    }
                }
            } else {
                payment = null;
                ISOMsg reversal = NonTagListGenerator.generateReversal(paymentRequest, "2400");
                payment = channelManager.sendMsg(reversal);
                if (payment != null) { //if no respon for reversal
                    if (payment.getString(39).equals("0000")) {
                        partnerDepositService.failed(transaksi, payment);
                        sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                    } else if (failedCode.containsKey(payment.getString(39))) {
                        reversal = NonTagListGenerator.generateReversal(paymentRequest, "2401");
                        payment = null;
                        payment = channelManager.sendMsg(reversal);
                        if (payment != null) {
                            if (payment.getString(39).equals("0000")) {
                                partnerDepositService.failed(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                            } else if(failedCode.containsKey(payment.getString(39))) {
                                reversal = NonTagListGenerator.generateReversal(paymentRequest, "2401");
                                payment = null;
                                payment = channelManager.sendMsg(reversal);
                                if (payment.getString(39).equals("0000")) {
                                    partnerDepositService.failed(transaksi, payment);
                                    sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                                }else{
                                    partnerDepositService.failed(transaksi, payment);
                                    sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                                }
                            }else{
                                partnerDepositService.failed(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                            }
                        }else{
                            partnerDepositService.failed(transaksi, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                        }
                    } else {
                        partnerDepositService.failed(transaksi, payment);
                        sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                    }
                }else{
                    reversal = NonTagListGenerator.generateReversal(paymentRequest, "2401");
                    payment = null;
                    payment = channelManager.sendMsg(reversal);
                    if (payment != null) {
                        if (payment.getString(39).equals("0000")) {
                            partnerDepositService.failed(transaksi, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                        } else if(failedCode.containsKey(payment.getString(39))) {
                            reversal = NonTagListGenerator.generateReversal(paymentRequest, "2401");
                            payment = null;
                            payment = channelManager.sendMsg(reversal);
                            if (payment.getString(39).equals("0000")) {
                                partnerDepositService.failed(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                            }else{
                                partnerDepositService.failed(transaksi, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                            }
                        }else{
                            partnerDepositService.failed(transaksi, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                        }
                    }else{
                        reversal = NonTagListGenerator.generateReversal(paymentRequest, "2401");
                        payment = null;
                        payment = channelManager.sendMsg(reversal);
                        if (payment.getString(39).equals("0000")) {
                            partnerDepositService.failed(transaksi, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                        }else{
                            partnerDepositService.failed(transaksi, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()));
                        }
                    }
                }
            }
        } catch (Exception e) {
            TLog.log(LOG + " EXEPTION - " + e.getMessage());
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            TLog.log(LOG + " EXEPTION STACK TRACE - " + e.toString());

            try {
                partnerDepositService.failed(transaksi, payment);
            } catch (ISOException e1) {
                e1.printStackTrace();
            }
            sendBack(ResponseCode.SERVER_UNAVAILABLE.setNtrans(transaksi.getNTRANS()));
        }

    }

    private void sendBack(ResponseCode r){
        if(response.isSuspended()){
            response.resume(Response.status(200).entity(r).build());
        } else {
            try {
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(r));
                httpPost.setEntity(entity);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                CloseableHttpResponse response = null;
                response = client.execute(httpPost);
                String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
                TLog.log("SEND API RESPONSE " + respone);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void sendBack(Response r){
        if(response.isSuspended()){
            response.resume(r);
        } else {
            try {
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(r.getEntity()));
                httpPost.setEntity(entity);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                CloseableHttpResponse response = null;
                response = client.execute(httpPost);
                String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
                TLog.log("SEND API RESPONSE " + respone);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private List<Rules> parsingRules(ISOMsg d, boolean status) {
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(d.getString(48))
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(48, status))
                .generate();
        List<Rules> bit61 = new SDE.Builder()
                .setPayload(d.getString(61))
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(61, status))
                .generate();
        List<Rules> bit62 = new SDE.Builder()
                .setPayload(d.getString(62))
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(62, status))
                .generate();
        bit48.addAll(bit61);
        bit48.addAll(bit62);
        bit48.add(new Rules(d.getString(63)));
        return bit48;
    }
}
