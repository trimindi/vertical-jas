package com.trimindi.switching.servindo;

/**
 * Created by PC on 21/07/2017.
 */
public class PaymentResponse {
    private boolean status = true;
    private String ntrans;
    private double totalBayar;
    private double saldo;
    private double totalFee;
    private PaketData data;

    public PaymentResponse() {
    }

    public boolean isStatus() {
        return status;
    }

    public PaymentResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public PaymentResponse setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public double getTotalBayar() {
        return totalBayar;
    }

    public PaymentResponse setTotalBayar(double totalBayar) {
        this.totalBayar = totalBayar;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public PaymentResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public double getTotalFee() {
        return totalFee;
    }

    public PaymentResponse setTotalFee(double totalFee) {
        this.totalFee = totalFee;
        return this;
    }

    public PaketData getData() {
        return data;
    }

    public PaymentResponse setData(PaketData data) {
        this.data = data;
        return this;
    }
}
