
package com.trimindi.switching.servindo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaketData {
    private String harga;
    private String kodeproduk;
    private String message;
    private String reg_id;
    private String saldo;
    private String sn;
    private int status;
    private Long timestamp;

    private String tn;

    public PaketData() {
    }

    @XmlTransient
    public String getHarga() {
        return harga;
    }

    public PaketData setHarga(String harga) {
        this.harga = harga;
        return this;
    }

    public String getKodeproduk() {
        return kodeproduk;
    }

    public PaketData setKodeproduk(String kodeproduk) {
        this.kodeproduk = kodeproduk;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public PaketData setMessage(String message) {
        this.message = message;
        return this;
    }
    @XmlTransient
    public String getReg_id() {
        return reg_id;
    }

    public PaketData setReg_id(String reg_id) {
        this.reg_id = reg_id;
        return this;
    }

    @XmlTransient
    public String getSaldo() {
        return saldo;
    }

    public PaketData setSaldo(String saldo) {
        this.saldo = saldo;
        return this;
    }

    public String getSn() {
        return sn;
    }

    public PaketData setSn(String sn) {
        this.sn = sn;
        return this;
    }
    @XmlTransient
    public int getStatus() {
        return status;
    }

    public PaketData setStatus(int status) {
        this.status = status;
        return this;
    }

    @XmlTransient
    public Long getTimestamp() {
        return timestamp;
    }

    public PaketData setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @XmlTransient
    public String getTn() {
        return tn;
    }

    public PaketData setTn(String tn) {
        this.tn = tn;
        return this;
    }
}
