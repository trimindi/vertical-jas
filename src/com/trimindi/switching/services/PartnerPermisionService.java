package com.trimindi.switching.services;

import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.PartnerCredential;
import com.trimindi.switching.models.PartnerPermision;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by PC on 21/07/2017.
 */
public class PartnerPermisionService {

    public PartnerPermisionService() {
    }

    public PartnerPermision getPermision(PartnerCredential partnerCredential) {
        Session session = SessionUtils.getSession();
        return session.find(PartnerPermision.class,partnerCredential.getPartner_uid());
    }
}
