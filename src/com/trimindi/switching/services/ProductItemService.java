package com.trimindi.switching.services;


import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.ProductItem;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by sx on 13/05/17.
 */
public class ProductItemService {

    public ProductItemService() {
    }


    public void persist(ProductItem entity) {
        Session session = SessionUtils.getSession();
        Transaction tx = session.beginTransaction();
        session.persist(entity);
        tx.commit();
        session.close();
    }

    public void update(ProductItem entity) {
        Session session = SessionUtils.getSession();
        Transaction tx = session.beginTransaction();
        session.update(entity);
        tx.commit();
        session.close();
    }

    public ProductItem findById(Long id) {
        Session session = SessionUtils.getSession();
        ProductItem book = session.get(ProductItem.class,id);
        session.close();
        return book;
    }
    public ProductItem findByDenom(String denom) {
        Session session = SessionUtils.getSession();
        Criteria criteria = session.createCriteria(ProductItem.class);
        criteria.add(Restrictions.eq("DENOM",denom));
        ProductItem book = (ProductItem) criteria.uniqueResult();
        session.close();
        return book;
    }

    public void delete(Long id) {
        Session session = SessionUtils.getSession();
        Transaction tx = session.beginTransaction();
        ProductItem pc = (ProductItem) session.get(ProductItem.class,id);
        session.delete(pc);
        tx.commit();
        session.close();
    }

    public List<ProductItem> findAll() {
        Session session = SessionUtils.getSession();
        Criteria criteria = session.createCriteria(ProductItem.class);
        List<ProductItem> q = criteria.list();
        session.close();
        return q;
    }
}
