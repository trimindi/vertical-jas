package com.trimindi.switching.services;

import com.trimindi.switching.SessionUtils;
import com.trimindi.switching.models.TransaksiDeposit;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by HP on 22/05/2017.
 */
public class TransaksiDepositService {
    public TransaksiDepositService() {
    }

    public List<TransaksiDeposit> findAll(String partnerid, String bulan, String tahun){
        Session session = SessionUtils.getSession();
        Criteria criteria = session.createCriteria(TransaksiDeposit.class);
        System.out.println("bulan :" + bulan);
        if(!bulan.equals("SEMUA")){
            criteria.add(Restrictions.eq("BULAN",bulan));
        }
        if (!tahun.equals("SEMUA")){
            criteria.add(Restrictions.eq("TAHUN",tahun));
        }
        criteria.add(Restrictions.eq("partner_id",partnerid));
        List<TransaksiDeposit> res = criteria.list();
        session.close();
        return res;
    }
}
