package com.trimindi.switching;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.NameRegistrar;

import java.io.File;

/**
 * Created by HP on 30/05/2017.
 */
public class SessionUtils extends QBeanSupport {
    private static SessionFactory sessionFactory;
    public SessionUtils() {
    }
    public static synchronized  SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            File hibernatePropsFile = new File("hibernate.cfg.xml");
            Configuration configuration = new Configuration();
            configuration.configure(hibernatePropsFile);
            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession() {
        return getSessionFactory().openSession();
    }

    @Override
    protected void initService() throws Exception {
        try {
            String hibernatePropsFilePath = cfg.get("hibernate_cfg_path");
            File hibernatePropsFile = new File(hibernatePropsFilePath);
            Configuration configuration = new Configuration();
            configuration.configure(hibernatePropsFile);
            sessionFactory = configuration.buildSessionFactory();
            NameRegistrar.register("hibernate-manager", this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
