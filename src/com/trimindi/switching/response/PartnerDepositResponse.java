package com.trimindi.switching.response;

import com.trimindi.switching.models.TransaksiDeposit;
import com.trimindi.switching.models.PartnerDeposit;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by HP on 22/05/2017.
 */
@XmlRootElement
public class PartnerDepositResponse {
    private boolean status;
    private PartnerDeposit partnerDeposit;
    private double totalFee;
    private List<TransaksiDeposit> transaksiDeposits;

    public double getTotalFee() {
        return totalFee;
    }

    public PartnerDepositResponse setTotalFee(double totalFee) {
        this.totalFee = totalFee;
        return this;
    }

    public PartnerDepositResponse() {
    }

    public boolean isStatus() {
        return status;
    }

    public PartnerDepositResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public PartnerDeposit getPartnerDeposit() {
        return partnerDeposit;
    }

    public PartnerDepositResponse setPartnerDeposit(PartnerDeposit partnerDeposit) {
        this.partnerDeposit = partnerDeposit;
        return this;
    }

    public List<TransaksiDeposit> getTransaksiDeposits() {
        return transaksiDeposits;
    }

    public PartnerDepositResponse setTransaksiDeposits(List<TransaksiDeposit> transaksiDeposits) {
        this.transaksiDeposits = transaksiDeposits;
        return this;
    }
}
