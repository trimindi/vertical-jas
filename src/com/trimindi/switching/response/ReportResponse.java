package com.trimindi.switching.response;

import com.trimindi.switching.models.Transaksi;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by HP on 23/05/2017.
 */
@XmlRootElement
public class ReportResponse {
    private boolean status;
    private List<Transaksi> transaksiList;

    public boolean isStatus() {
        return status;
    }

    public ReportResponse() {
    }

    public ReportResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public List<Transaksi> getTransaksiList() {
        return transaksiList;
    }

    public ReportResponse setTransaksiList(List<Transaksi> transaksiList) {
        this.transaksiList = transaksiList;
        return this;
    }
}
