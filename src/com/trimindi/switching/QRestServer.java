package com.trimindi.switching;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.trimindi.switching.client.ChannelManager;
import com.trimindi.switching.exeption.*;
import com.trimindi.switching.filter.CORSFilter;
import com.trimindi.switching.filter.SecureEndpoint;
import com.trimindi.switching.packager.PLNPackager;
import com.trimindi.switching.utils.generator.BaseHelper;
import com.trimindi.switching.utils.generator.PrePaidGenerator;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.jdom.Element;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.q2.QBeanSupport;
import org.jpos.q2.QFactory;
import org.jpos.util.NameRegistrar;

import javax.xml.bind.JAXB;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by sx on 11/05/17.
 */
public class QRestServer extends QBeanSupport implements QRestServerMBean {
    private Server server;
    private int port;
    private ChannelManager manager;
    public QRestServer() {
        super();
    }

    @Override
    protected void startService() throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        ResourceConfig resourceConfig = resourcesConfig();
        resourceConfig.register(CORSFilter.class);
        resourceConfig.register(SecureEndpoint.class);
        resourceConfig.register(JacksonJaxbJsonProvider.class);
        resourceConfig.register(JAXB.class);
        resourceConfig.register(CustomExeption.class);
        resourceConfig.register(CustomBadRequestExeption.class);
        resourceConfig.register(CustomNotAllowedException.class);
        resourceConfig.register(CustomInvalidJsonParseExeption.class);
        resourceConfig.register(CustomNotFoundExeption.class);

        context.addServlet(new ServletHolder(new ServletContainer(resourceConfig)), "/*");
        server = new Server(port);
        server.setHandler(context);
        server.start();
        NameRegistrar.register(getName(), this);
        try {
            manager = (ChannelManager) NameRegistrar.get("manager");
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ISOMsg msg = manager.sendMsg(PrePaidGenerator.generateNetworkSignOn());
                    if (msg != null){

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

        try {
            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            BufferedReader bufferedReader = new BufferedReader(new FileReader("stan-"+date+".resume"));
            BaseHelper.setCounter(Integer.parseInt(bufferedReader.readLine()));
        } catch (IOException e) {
            BaseHelper.writeStanNumber(1);
            BaseHelper.setCounter(1);
        }
    }

    @SuppressWarnings("rawtypes")
    private ResourceConfig resourcesConfig() throws Exception {
        ResourceConfig resourceConfig = new ResourceConfig();

        QFactory factory = getFactory();
        Iterator iterator = getPersist().getChildren("rest-listener").iterator();
        while (iterator.hasNext()) {
            Element l = (Element) iterator.next();

            Class<?> cl = Class.forName(l.getAttributeValue ("class"));
            Constructor<?> cons = cl.getConstructor(ISOPackager.class);

            String packagerName = l.getAttributeValue ("packager");
            ISOPackager packager = null;
            if (packagerName != null) {
                packager = (ISOPackager) factory.newInstance (packagerName);
            }
            RestListener listener = (RestListener) cons.newInstance(packager);

            factory.setLogger (listener, l);
            factory.setConfiguration (listener, l);

            resourceConfig.register(listener);
        }
        return resourceConfig;
    }

    @Override
    protected void stopService() throws Exception {
        new Thread(new Runnable() {
            @Override
            public void run() {
            try {
                ISOMsg msg = manager.sendMsg(PrePaidGenerator.generateNetworkSignOFF());
                if (msg != null){
                    msg.setPackager(new PLNPackager());
                    msg.dump(System.out,"Network SIGN OFF Call");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            }
        }).start();
        server.stop();
        server.destroy();
        NameRegistrar.unregister(getName());
        BaseHelper.saveStan();
    }

    @Override
    public int getPort() {
        return this.port;
    }

    @Override
    public void setPort(int port) {
        this.port = port;
    }
}
