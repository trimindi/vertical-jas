package com.trimindi.switching.utils.constanta;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.ws.Response;

/**
 * Created by sx on 13/05/17.
 */
@XmlRootElement
public class ResponseCode {
    public static final ResponseCode SERVER_UNAVAILABLE = new ResponseCode("9999","Server Unavailable");
    public static final ResponseCode SERVER_TIMEOUT = new ResponseCode("9998", "Request Timeout");
    public static final ResponseCode PARAMETER_MERCHANT_TYPE = new ResponseCode("9997","Merchant type tidak di kenali");
    public static final ResponseCode PARAMETER_SALDO = new ResponseCode("1003","Saldo anda tidak mencukupi.");
    public static final ResponseCode PARAMETER_UNKNOW_DENOM = new ResponseCode("1002","Kode denom tidak di kenali");
    public static final ResponseCode NTRANS_NOT_FOUND = new ResponseCode("1006","Nomor transaksi tidak di temukan");
    public static final ResponseCode MISSING_REQUIRED_PARAMETER = new ResponseCode("8999","Missing Required paramater");
    public static final ResponseCode PAYMENT_UNDER_PROSES = new ResponseCode("1007","Pembayaran sedang dalam proses..");
    public static final ResponseCode PAYMENT_DONE = new ResponseCode("1008","Ntrans sudah tidak berlaku pembayaran berhasil");
    public static final ResponseCode PAYMENT_FAILED = new ResponseCode("1009","Pembelian Gagal");
    public static final ResponseCode MAXIMUM_PRINTED_RECIPT = new ResponseCode("1010", "Melebihi batas maksimal print struk");
    public static final ResponseCode WRONG_NTRANS = new ResponseCode("1030", "Ntrans tidak terdaftar");
    public static final ResponseCode NTRANS_NOT_MATCH_WITH_CA_REFF = new ResponseCode("1041","Ntrans dan Referensi id tidak sama");
    public static final ResponseCode UNKNOW_ACTION = new ResponseCode("2000", "Unknow action");
    public static final ResponseCode INVALID_BODY_REQUEST_FORMAT = new ResponseCode("2001","Invalid body request");
    public static final ResponseCode PRODUCT_UNDER_MAINTANCE = new ResponseCode("2002", "Product Under Maintance");
    public static final ResponseCode PERMISION_USER_DENIED = new ResponseCode("2011", "Partner tidak memiliki permision action");
    public static final ResponseCode PARAMETER_INVALID_PRODUCT_ACTION = new ResponseCode("2012", "Invalid Action and procut");
    private boolean status;
    private String code;
    private String message;
    private String ntrans;

    public ResponseCode() {
    }

    @XmlElement(name = "ntrans")
    public String getNtrans() {
        return ntrans;
    }

    public ResponseCode setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public ResponseCode(String code, String message) {
        this.code = code;
        this.status = false;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public ResponseCode setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public String getCode() {
        return code;
    }

    public ResponseCode setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseCode setMessage(String message) {
        this.message = message;
        return this;
    }
}
