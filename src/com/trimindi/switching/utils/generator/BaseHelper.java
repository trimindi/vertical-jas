package com.trimindi.switching.utils.generator;

import org.apache.commons.lang3.StringUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by sx on 13/05/17.
 */
public class BaseHelper {
    public static final String PAN_PREPAID = "99502";
    public static final String PAN_POSTPAID = "99501";
    public static final String PAN_NONTAGLIST = "99504";
    public static final String PARTNER_ID = "4410101";
    public static final String BANK_CODE = "4410010";
    public static final String TERMINAL_ID = "NONAJASV00000001";
    public static final String SWITCHER_ID = "0000000";


    public static final String SIGN_ON = "001";
    public static final String SIGN_OFF = "002";
    public static final String ECHO_TEST = "003";
    private static int counter = 0;
    private static String date;

    public static void setCounter(int x){
        counter = x;
    }

    public static String date14() {
        return new SimpleDateFormat("YYYYMMDDhhmm").format(new Date()) +""+new SimpleDateFormat("ss").format(new Date()).substring(0,1);
    }

    public static String date8()
    {
        return new SimpleDateFormat("ddhhmmss").format(new Date());
    }


    public static synchronized String stanDB(){
        return StringUtils.leftPad(String.valueOf(counter++),12,"0");
    }
    public static String padLeftWithZero(String value,int length){
        return StringUtils.leftPad(value,length,"0");
    }

    public static String padRightWithZero(String value,int length){
        return StringUtils.rightPad(value,length,"0");
    }
    public static String padLeftWithSpace(String value,int length){
        return StringUtils.leftPad(value,length," ");
    }

    public static String padRightWithSpace(String value,int length){
        return StringUtils.rightPad(value,length," ");
    }

    public static String numberToPay(double number){
        int a = (int) number;
        return String.valueOf(a);
    }

    public static String amountToPay(double amount) {
        DecimalFormat decimalFormat = new DecimalFormat();
        String curency = "360";
        String minor = "";
        String harga ="";
        harga = new DecimalFormat("#").format(amount);
        return curency + minor + StringUtils.leftPad(harga,12,"0");
    }

    public static String valueToMinor(double amount,int minor) {
        if(amount % 1 == 0){
            StringBuilder s = new StringBuilder();
            for(int i=1;i<= minor;i++){
                s.append("0");
            }

            return new DecimalFormat("#").format(amount) + s.toString();
        }else{
            StringBuilder s = new StringBuilder();
            s.append("1");
            for(int i=1;i<= minor;i++) {
                s.append("0");
            }
            return new DecimalFormat("#").format(amount * Integer.parseInt(s.toString()));
        }
    }

    public double numberMinorUnit(String val, int min) {
        int bagi = Integer.parseInt("1" + StringUtils.leftPad("", min, "0"));
        return Double.parseDouble(val) / bagi;
    }

    public static void writeStanNumber(int i) {
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            FileWriter fileWriter = new FileWriter("stan-"+date+"resume");
            fileWriter.write(i);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveStan() {
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            FileWriter fileWriter = new FileWriter("stan-"+date+"resume");
            fileWriter.write(counter);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
