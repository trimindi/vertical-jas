package com.trimindi.switching.client;

import org.jpos.iso.ISOMsg;
import org.jpos.iso.MUX;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.Logger;
import org.jpos.util.NameRegistrar;

/**
 * Created by sx on 12/05/17.
 */
public class ChannelManager extends QBeanSupport {
    private long MAX_TIME_OUT;
    private MUX mux;

    @Override
    protected void initService() throws Exception {
        try {
            mux = (MUX) NameRegistrar.get("mux." + cfg.get("mux"));
            MAX_TIME_OUT = cfg.getLong("timeout");
            NameRegistrar.register("manager", this);
        } catch (NameRegistrar.NotFoundException e) {
            System.out.println("nameregister not found");
            log.error("Error in initializing service :" + e.getMessage());
        }
    }
    public ISOMsg sendMsg(ISOMsg m)
            throws Exception {
        return sendMsgPLN(m, mux, MAX_TIME_OUT);
    }

    private ISOMsg sendMsgPLN(ISOMsg msg, MUX mux, long time)
            throws Exception {
        if (mux != null) {
            ISOMsg respMsg = mux.request(msg, time);;
            return respMsg;
        }
        return null;
    }
}
