package com.trimindi.switching.client;

import com.trimindi.switching.utils.generator.PrePaidGenerator;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.util.SimpleLogListener;

import java.io.IOException;

/**
 * Created by sx on 12/05/17.
 */
public class ClientRequestListener implements ISORequestListener {
    private static org.jpos.util.Logger logger = new org.jpos.util.Logger();
    public ClientRequestListener() {
        super();
        logger.addListener(new SimpleLogListener(System.out));
    }

    @Override
    public boolean process(ISOSource isoSrc, ISOMsg isoMsg) {
        try {
            isoSrc.send(PrePaidGenerator.generateNetworkSignOn());
        } catch (ISOException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
