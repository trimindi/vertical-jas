package test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.rajabiller.response.MethodResponse;
import com.trimindi.switching.response.telkom.PaymentTelkom;
import com.trimindi.switching.services.PartnerDepositService;
import com.trimindi.switching.services.ProductFeeService;
import com.trimindi.switching.services.ProductItemService;
import com.trimindi.switching.services.TransaksiService;
import com.trimindi.switching.utils.TLog;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.junit.Before;
import org.junit.Test;

import javax.net.ssl.SSLContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Created by PC on 05/07/2017.
 */
public class Telkom {
    private static JAXBContext jc;
    private static Unmarshaller unmarshaller;
    ObjectMapper objectMapper;

    @Before
    public void before() {
        try {
            objectMapper = new ObjectMapper();
            jc = JAXBContext.newInstance(MethodResponse.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            TLog.log("ERRR" + e.getMessage());
        }

    }

    @Test
    public void test() throws JsonProcessingException {
        String respone = null;
        PaymentTelkom payment = null;
        respone = "<?xml version=\"1.0\"?>\n" +
                "<methodResponse>\n" +
                "<params>\n" +
                "<param>\n" +
                "<value><array>\n" +
                "<data>\n" +
                "<value><string>SPEEDY</string></value>\n" +
                "<value><string>20170630212020</string></value>\n" +
                "<value><string>152305102414</string></value>\n" +
                "<value><string>152305102414</string></value>\n" +
                "<value><string>152305102414</string></value>\n" +
                "<value><string>RYCKY RAHMAWAN ABDI</string></value>\n" +
                "<value><string>201706</string></value>\n" +
                "<value><string>207610</string></value>\n" +
                "<value><string>2500</string></value>\n" +
                "<value><string>SP90212</string></value>\n" +
                "<value><string>974048</string></value>\n" +
                "<value><string>3D3C6F07EE524E228A36BA8C13145E08</string></value>\n" +
                "<value><string>753735802</string></value>\n" +
                "<value><string>0</string></value>\n" +
                "<value><string>00</string></value>\n" +
                "<value><string>APPROVE</string></value>\n" +
                "<value><string>208610</string></value>\n" +
                "<value><string>4643374</string></value>\n" +
                "<value><string>https://202.43.173.234/struk/?id=rE%2BuuKtNOaNYo%2F7p7MYfCK%2BFJISIbTGUg3WagD5ksGsb%2BSXe8%2BcA9m7w6087AN4Dkc2qD17V6c7H53QAfwQnpw%3D%3D</string></value>\n" +
                "<value><struct>\n" +
                "<member><name>CATATAN</name>\n" +
                "<value><string></string></value>\n" +
                "</member>\n" +
                "<member><name>JUMLAHBILL</name>\n" +
                "<value><string>1</string></value>\n" +
                "</member>\n" +
                "</struct></value>\n" +
                "</data>\n" +
                "</array></value>\n" +
                "</param>\n" +
                "</params>\n" +
                "</methodResponse>";
        System.out.println(respone);
        MethodResponse methodResponse = null;
        StringReader stringReader = new StringReader(respone);
        try {
            methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
            payment = new PaymentTelkom(methodResponse);
            String res = objectMapper.writeValueAsString(payment);
            System.out.println(res);

        } catch (JAXBException e) {
            e.printStackTrace();
            TLog.log("ERRR" + e.getMessage());
        }
    }

}
