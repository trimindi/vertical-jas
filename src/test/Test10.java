package test;

import com.trimindi.switching.controllers.Request;
import com.trimindi.switching.response.prepaid.InquiryResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by PC on 19/07/2017.
 */
public class Test10 {


    @Test
    public void prepaidTest () throws InterruptedException {
        String userid = "VER01";
        String password = "V$2309485";
        String mt = "6021";
        String time = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());

        String pass = DigestUtils.sha1Hex(userid + time + password + mt);
        System.out.println("time " + time);
        System.out.println("pass" + pass);

    }
}
