package test;

import com.trimindi.switching.response.postpaid.Rincian;
import com.trimindi.switching.utils.generator.BaseHelper;
import com.trimindi.switching.utils.iso.models.Rules;
import com.trimindi.switching.utils.iso.parsing.SDE;
import com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC on 30/07/2017.
 */
public class StanTest {

    @Test
    public void testStan(){
        String bit = "00000005621000245391101144121C9B57F8F1D192A5088E69C2320PJU PEMDA KAB SERANG     56210000000000000000P3  0000022000000000002017072007201700000000000000355147D00000000000000000000000000000000000033880000362800000000000000000000000000000000";
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(bit)
                .setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, true))
                .generate();
        int legth = new SDE.Builder()
                .setPayload(bit)
                .setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, true))
                .calculate();
        System.out.println(bit48);
        System.out.println(legth);
        bit = bit.substring(legth,bit.length());
        System.out.println(bit.length());
        int start = 0;
        int leghtRincian = 115;
        List<Rincian> rincians = new ArrayList<>();
        for(int i = 0; i< 1; i++){
            String parsRincian = bit.substring(start,start+leghtRincian);
            List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
            Rincian r = new Rincian(rc);
            rincians.add(r);
            start += leghtRincian;
        }
    }
}
